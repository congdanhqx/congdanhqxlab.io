#!/bin/sh

# Change this directory to Git's source code
# git should be built already
: ${GIT_SRC_DIR:=$HOME/src/git}

git() {
	"$GIT_SRC_DIR/git" --exec-path="$GIT_SRC_DIR" -c protocol.version=2 "$@"
}

manipulate() {
	(
	cd "$1"
	# Clean other refs
	git reset --hard v5.4
	git for-each-ref refs/tags |
		cut -d/ -f3 |
		xargs git tag --delete
	git remote add stable git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
	git fetch stable v5.8.10
	git merge v5.8.10
	git fsck
	git gc --prune=now
	)
}

# Clone from mainline
# This will be upstream
git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

# Clone to 2 different downstreams
git clone --no-local linux linux-no-bloom
git clone --no-local linux linux-bloom

cleanref linux-no-bloom
cleanref linux-bloom

rm -f bloom.trace no-bloom.trace
(
	cd linux-no-bloom
	GIT_PACKET_TRACE=$(pwd)/../no-bloom.trace \
		git -c transfer.bloom=0 fetch "$(pwd)/../linux" v5.10
)
(
	cd linux-bloom
	GIT_PACKET_TRACE=$(pwd)/../bloom.trace \
		git -c transfer.bloom=1 fetch "$(pwd)/../linux" v5.10
)

# Packet In
awk '/sideband/{exit}; $4~/<$/{ $1=""; print }' bloom.trace >bloom.in
awk '/sideband/{exit}; $4~/<$/{ $1=""; print }' no-bloom.trace >no-bloom.in
# Packet Out
awk '/sideband/{exit}; $4~/>$/{ $1=""; print }' bloom.trace >bloom.out
awk '/sideband/{exit}; $4~/>$/{ $1=""; print }' no-bloom.trace >no-bloom.out
# Negotation time
printf "Negotiation with    Bloom: "
sed '/sideband/{print $1; exit}' bloom.trace
printf "Negotiation without Bloom: "
sed '/sideband/{print $1; exit}' no-bloom.trace
