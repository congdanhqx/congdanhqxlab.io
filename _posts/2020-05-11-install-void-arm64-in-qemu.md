---
layout: post
title: Install Void Linux for AArch64 in QEMU
comments: false
categories:
  - tips
tags:
  - VoidLinux
  - qemu
---

Recalled from memory, I haven't double checked.

# Prepare parallel flash image:

```sh
$ curl -LO http://releases.linaro.org/components/kernel/uefi-linaro/latest/release/qemu64/QEMU_EFI.fd
$ dd if=QEMU_EFI.fd of=flash0.img conv=notrunc
```

# Fetch Debian Live CD for arm64

I haven't figured out how to use `void-mklive`, yet.

```sh
$ curl -LO https://cdimage.debian.org/debian-cd/current/arm64/iso-cd/debian-10.4.0-arm64-netinst.iso  
```

# Prepare Virtual Disk:

```sh
$ : prepare 10G virtual disk
$ dd if=/dev/zero of=/files/arm64.img bs=1M count=10240
```

# Boot the Live CD:

```sh
$ qemu-system-aarch64 \
    -M virt \
    -cpu cortex-a53 \
    -pflash flash0.img \
    -cpu cortex-a53 \
    -smp 4 \
    -no-reboot \
    -nographic \
    -m 2G \
    -serial mon:stdio \
    -drive file=debian-10.4.0-arm64-netinst.iso,id=cdrom,if=none,media=cdrom \
    -device virtio-scsi-device -device scsi-cd,drive=cdrom \
    -drive file=/files/arm64.img,format=raw,if=none,id=hd0 \
    -device virtio-blk-device,drive=hd0 \
```

# Load required kernel module:

Using Debian Expert Install curses interface and select is the easiest.
I didn't dig into this.

# Disk Partition VoidLinux

We need at least 2 partitions, 1 for efi bootloader, 1 for `/`

```sh
$ fdisk /dev/vda
$ mkfs.ext4 /dev/vda2
$ mkfs.vfat /dev/vda1
$ mount -t ext4 /dev/vda2 /mnt
```

If using GRUB as bootloader:

```sh
$ mkdir -p /mnt/boot/efi
$ mount -t vfat /dev/vda1 /mnt/boot/efi
```

If using rEFInd as bootloader

```sh
$ mkdir -p /mnt/boot
$ mount -t vfat /dev/vda1 /mnt/boot
```

# Fetch latest XBPS static build:

As of 2020-05-11, latest XBPS is at version `0.59_5`.
TODO: write a script to fetch it.

```sh
$ cd /
$ curl -LO https://alpha.de.repo.voidlinux.org/static/xbps-static-static-0.59_5.aarch64-musl.tar.gz
$ tar xzf xbps-static*
```

# Install base packages:

```sh
$ : additional packages for GRUB
$ more_pkg=grub-arm64-efi
$ : additional packages for rEFInd
$ more_pkg=rEFInd
$ xbps-install.static \
    -R https://alpha.de.repo.voidlinux.org/current/aarch64 \
    -r /mnt -Su base-system dracut linux $more_pkg
$ mkdir -p /mnt/proc /mnt/sys /mnt/dev /mnt/tmp
$ mount -t proc proc /mnt/proc
$ mount -t sysfs sysfs /mnt/sys
$ mount -t devtmpfs devtmpfs /mnt/dev
$ mount -t tmpfs tmpfs /mnt/tmp
$ mkdir -p /mnt/dev/pts /mnt/dev/shm /mnt/var/tmp
$ mount -t devpts devpts /mnt/dev/pts
$ mount -t tmpfs tmpfs /mnt/var/tmp
$ mount -t tmpfs tmpfs /mnt/dev/shm
$ cp /etc/resolv.conf /mnt/etc
$ chroot /mnt
$ xbps-reconfigure -f -a
```

# Install bootloader:

## With GRUB

```sh
$ grub-install /dev/vda
$ cp /boot/efi/EFI/void /boot/efi/EFI/boot
$ cp /boot/efi/EFI/boot/grubaa64.efi /boot/efi/EFI/boot/bootaa64.efi
```

## With rEFInd

```sh
$ refind-install --usedefault /dev/vda1
$ cp /boot/EFI/BOOT/refind_aa64.efi /boot/EFI/BOOT/bootaa64.efi
```

# Enjoy

Use the same command to bring up the Live CD, remove everything related to
Live CD
