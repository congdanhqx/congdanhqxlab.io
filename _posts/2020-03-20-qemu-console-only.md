---
layout: post
title: QEMU - start Linux Virtual Machine without graphical UI
comments: false
categories:
  - tips
tags:
  - qemu
---

Command listed here was re-constructed from current configuration.
Please adjust it if it were wrong.

# Bootloader:

## For those that use ExtLinux (Alpine)

```sh
# sed -i.bak \
	-e '/^default_kernel_opts=/s/"[[:space:]]*$/ console=ttyS0,9600"/' \
	-e '/^serial_port=/ s/=.*/=0/' \
	-e '/^serial_baud=/ s/=.*/=9600/' \
	/etc/update-extlinux.conf
```

## For those that use Grub (VoidLinux, ArchLinux, Debian)

```sh
# sed -i.bak \
	-e '/GRUB_CMDLINE_LINUX_DEFAULT/ s/"$/ console=ttyS0,9600"/' \
	-e '/GRUB_TERMINAL_INPUT=/ s/.*/GRUB_TERMINAL_INPUT=console/' \
	-e '/GRUB_TERMINAL_OUTPUT=/ s/.*/GRUB_TERMINAL_OUTPUT=console/' \
	-e '/GRUB_TERMINAL=/ s/.*/GRUB_TERMINAL=console/' \
	/etc/default/grub
```

# Enable ttyS0

## OpenRC (Alpine)

Enable getty for ttyS0:

```sh
# grep -q "^ttyS0::" /etc/inittab ||
	sed -i.bak \
		-e '/^tty1/a\
ttyS0::respawn:/sbin/getty -L ttyS0 9600 vt100' \
	/etc/inittab
```

Mark ttyS0 is secure:

```sh
# grep -qx ttyS0 /etc/securetty ||
	echo ttyS0 >/etc/securetty
$ echo ttyS0 | sudo tee /etc/securetty
```

## runit (VoidLinux)

```sh
# ln -sf /etc/sv/agetty-ttyS0 /var/service
```

## SystemD (ArchLinux, Debian)

```sh
# systemctl enable serial-getty@ttyS0
```
