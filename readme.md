---
layout: post
permalink: /about.html
title: About this blog
comments: false
categories:
  - info
---

This is the blog of Danh Doan.

I am a normal developer.

I mostly work with C and C++ in a Linux environments.

Most of things inside this blog is taken from my daily work, life,
relationship.

Occasionally, there will be some rants.

My GnuPG key is [0x1CA8DFAC890801B5](1CA8DFAC890801B5.asc).

Its fingerprint is: 5735 40C3 C54E 025E 1F2B  0223 1CA8 DFAC 8908 01B5

My resume in [pdf format](danh.pdf) or [tex format](danh.tex).

#### This website:

- officially hosted at: <https://danh.dev>

- history is tracked at: <https://gitlab.com/sgn/sgn.gitlab.io>


#### Disclaimer:

- I have no idea what kind of craps am I saying.
Those posts are full of craps, they're neither fact or advice.
Don't trust those posts.
- My employer doesn't have any ideas about those craps either.
- Some (all?) contents are based on my imagination.
- I don't have liability with your data, programs, computers, whatsoever.

#### Privacy Notice:

- This blog doesn't use javascript, cookie, and/or whatsoever method to track you.
- This blog doesn't track your IP Address, but this blog is hosted by GitLab,
they may record and track your IP Address, please ask GitLab for their privacy
notice on that matter. Your DNS server may track you based on your DNS query.
Again, please ask your DNS provider for their privacy policy.
- I reserve the right to add other components (comments, etc..) to this blog
in future, this notice will be added if necessary.

#### LICENSE:

- Originally, I'd written this blog based on template from Guilherme Henrique,
  which in turn based on [Jacopo Rabolini's emerald][1].
- As of 2019-11-19, I removed all works (including but not limited to
  templates, contents) by Jacopo Rabolini and Guilherme Henrique.
- git blame still shows some lines written by Guilherme, those lines is part
  of `Gemfile` and `.gitlab-ci.yml` (GitLab's named tags), which isn't
  copyrightable.
- Everything in this blog is solely copyrighted by me.
- The blog contents is copyrighted by me, and licensed under
[Attribution-NonCommercial-ShareAlike 3.0 United States (CC BY-NC-SA 3.0 US)][2]
- It means:

    You are free to:
    1. **Share** — copy and redistribute the material in any medium or format
    2. **Adapt** — remix, transform, and build upon the material

    Under the following terms:
    1. **Attribution** — You must give appropriate credit,
    provide a link to the license, and indicate if changes were made.
    You may do so in any reasonable manner,
    but not in any way that suggests the licensor endorses you or your use.
    2. **NonCommercial** — You may not use the material for commercial purposes.
    3. **ShareAlike** — If you remix, transform, or build upon the material,
    you must distribute your contributions under the same license as the original.

For any reuse or distribution of my work,
you must make clear to others the license terms of this work.
The best way to do this is with a link to this web page.

Because my contents is for non-commercial purposes,
you may not make money off of my contents without my express written permission.
This means no advertising is allowed on your site without my consents.

[1]: https://github.com/KingFelix/emerald
[2]: https://creativecommons.org/licenses/by-nc-sa/3.0/us/
